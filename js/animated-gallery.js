(function ($) {
  $.fn.animatedGallery = function (options) {

    // default options
    var defaults = {
      galleryWidth: '900px',
      wrapperBackground: 'red'
    };

    // extend default options
    var options = $.extend({}, defaults, options);

    return this.each(function () {

      var $this = $(this);

      var $thisUl = $this.find('ul');
      var $thisLi = $this.find('li');


      $this.wrapAll('<div class="gallery-wrapper"></div>');

      $('div:first').before('<div class="gallery-dim"></div>');


      var liSize = $thisLi.size();

      var totalWidth = 0;


      var imagesArray = new Array();
      $('img', $thisLi).each(function () {
        theWidth = $(this).width();
        theHeight = $(this).height();

        $(this).attr({
          'height': theHeight,
          'width': theWidth
        });


        theImg = $(this);
        imagesArray.push(theImg);
        totalWidth = totalWidth + $(this).width() + 5;
      });

      var correctLines = Math.round(totalWidth / parseInt(options.galleryWidth))
      var linesNumber = Math.round(liSize / correctLines);

      console.log(correctLines);
      console.log(totalWidth);
      console.log(linesNumber);
      console.log(liSize);

      var indexedLi = 1;
      var indexed = 0;
      $this.find('li').each(function () {
        if ((indexed < linesNumber)) {
          $(this).addClass('splitter_' + indexedLi);
          indexed++;
        } else {
          indexedLi++;
          indexed = 1;
          $(this).addClass('splitter_' + indexedLi);
        }
      });


      for (var i = 0; i < linesNumber; i++) {
        $this.find('.splitter_' + i).insertBefore($thisUl).wrapAll('<ul class="ul-index_' + i + '"></ul>');
      };

      $this.css({
        'width': options.galleryWidth,
        'margin': '0 auto'
      });

      $('ul', $this).css({
        'position': 'relative',
        'list-style': 'none',
        'margin': '0',
        'padding': '0',
        'width': options.galleryWidth,
        'text-align': 'center'
      });

      $('li', $this).css({
        'display': 'inline-block',
        'padding': '1px',
        'margin': '1px',
        'cursor': 'pointer'
      });

      $('.gallery-wrapper').css({
        'background': options.wrapperBackground,
        'overflow': 'hidden',
        'padding': '20px 0'
      });


      $('.gallery-dim').css({
        'display': 'none',
        'position': 'absolute',
        'width': '100%',
        'height': '100%',
        'top': 0,
        'left': 0,
        'background-color': 'white',
        'z-index': '1000'
      });

      function isEven(n) {
        return n && (n % 2 == 0);
      }


      $('ul', $this).each(function (i) {
        var documentWidth = $(window).width();
        if (isEven(i + 1)) {
          $(this).css('left', documentWidth);
        } else {
          $(this).css('right', documentWidth);
        }
      });


      var animation_index = 1;
      var animation = setInterval(function () {
        var randomNumber = 500 + Math.floor(Math.random() * 1000);
        if (isEven(animation_index)) {
          $('.ul-index_' + animation_index).animate({
            'left': 0
          }, {
            duration: randomNumber,
            queue: false
          });
        } else {
          $('.ul-index_' + animation_index).animate({
            'right': 0
          }, {
            duration: randomNumber,
            queue: false
          });
        }
        animation_index++;

        if (animation_index === linesNumber) {
          clearInterval(animation);
        }

      }, 100);


      // interactions
      $('<div class="the-show"></div>').appendTo('body');

      $('.the-show').css({
        'display': 'none',
        'position': 'absolute',
        'border': '1px solid black',
        'top': '10%',
        'left': '50%',
        'margin-left': -50,
        'z-index': '1100',
        'border': '15px solid white',
        'box-shadow': '0 0 5px #c0c0c0',
        'border-radius': '5px'
      });

      var theImage;
      $('li', $this).on('click', function () {
        theImage = $('img', this);

        theImage.clone().appendTo('.the-show');

        $('.the-show img').attr({
          'width' : '',
          'height' : ''
        });

        $('.gallery-dim').fadeTo(500, 0.8, function () {
          $('.the-show').fadeTo(500, 1);
        });
      });

      $(document).on('click', '.gallery-dim', function () {
        $('.the-show').fadeOut(500, function () {
          $('img', this).remove();
        });
        $('.gallery-dim').fadeOut(500);
        console.log(theImage);
      });

    });

  };

})(jQuery);
